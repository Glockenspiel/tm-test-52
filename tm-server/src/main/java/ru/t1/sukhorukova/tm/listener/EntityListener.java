package ru.t1.sukhorukova.tm.listener;

import lombok.NoArgsConstructor;
import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.log.OperationType;
import ru.t1.sukhorukova.tm.log.OperationEvent;

@NoArgsConstructor
public final class EntityListener implements
        PostInsertEventListener,
        PostDeleteEventListener,
        PostUpdateEventListener
{

    @NotNull
    private JmsLoggerProducer jmsLoggerProducer;

    public EntityListener(JmsLoggerProducer jmsLoggerProducer) {
        this.jmsLoggerProducer = jmsLoggerProducer;
    }

    @Override
    public void onPostDelete(PostDeleteEvent postDeleteEvent) {
        log(OperationType.DELETE, postDeleteEvent.getEntity());
    }

    @Override
    public void onPostInsert(PostInsertEvent postInsertEvent) {
        log(OperationType.INSERT, postInsertEvent.getEntity());
    }

    @Override
    public void onPostUpdate(PostUpdateEvent postUpdateEvent) {
        log(OperationType.UPDATE, postUpdateEvent.getEntity());
    }

    @Override
    public boolean requiresPostCommitHanding(EntityPersister entityPersister) {
        return false;
    }

    private void log(final OperationType operationType, final Object entity) {
        System.out.println(operationType + ": " + entity.hashCode());

        if (jmsLoggerProducer == null) return;
        jmsLoggerProducer.send(new OperationEvent(operationType, entity));
    }

}
