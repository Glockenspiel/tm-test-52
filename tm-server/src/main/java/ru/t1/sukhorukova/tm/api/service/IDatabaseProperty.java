package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseLogin();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHBM2DDLAuto();

    @NotNull
    String getDatabaseShowSQL();

    @NotNull
    String getDatabaseFormatSQL();

    @NotNull
    String getDatabaseSecondLvlCash();

    @NotNull
    String getDatabaseFactoryClass();

    @NotNull
    String getDatabaseQueryCashKey();

    @NotNull
    String getDatabaseUseMinPuts();

    @NotNull
    String getDatabaseRegionPrefix();

    @NotNull
    String getDatabaseConfigFilePath();

}
