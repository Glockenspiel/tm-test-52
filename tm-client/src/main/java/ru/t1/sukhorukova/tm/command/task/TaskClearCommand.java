package ru.t1.sukhorukova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.dto.request.task.TaskClearRequest;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-clear";

    @NotNull
    public static final String DESCRIPTION = "Remove all tasks.";

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");

        getTaskEndpoint().clearTask(new TaskClearRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
