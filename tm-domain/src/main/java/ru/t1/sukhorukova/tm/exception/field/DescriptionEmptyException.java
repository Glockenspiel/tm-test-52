package ru.t1.sukhorukova.tm.exception.field;

public final class DescriptionEmptyException extends AbstractFieldExceprion {

    public DescriptionEmptyException() {
        super("Error! Description is empty...");
    }

}
