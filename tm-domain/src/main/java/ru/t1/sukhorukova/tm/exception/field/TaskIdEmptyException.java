package ru.t1.sukhorukova.tm.exception.field;

public final class TaskIdEmptyException extends AbstractFieldExceprion {

    public TaskIdEmptyException() {
        super("Error! Task id is empty...");
    }

}
